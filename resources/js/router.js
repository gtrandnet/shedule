import VueRouter from 'vue-router';

import Rooms from './components/Rooms';
import AdminPanel from './components/AdminPanel';

export default new VueRouter({
    routes: [
    {
        path: '/admin/home',
        component: AdminPanel 
    }
],
    mode: 'history'
});