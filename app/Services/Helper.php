<?php

namespace App\Services;

use App\RoomReservation;
use Carbon\Carbon;
use App\Partner;


class Helper
{
    /**
     * @param  int $start, $finish, $rooms_id
     * @return response for validation date booking 
     */


    public static function isValid($start, $finish, $rooms_id, $id=Null)
    {
        if($id != Null)
        {
            $rooms = RoomReservation::where('room_id', $rooms_id)
                                                                ->where('id', '!=', $id)
                                                                ->where('status', '!=', 0)
                                                                ->get()->toArray();
        }else{
            $rooms = RoomReservation::where('room_id', $rooms_id)
                                                                ->where('status', '!=', 0)
                                                                ->get()->toArray();
        }

		$res = true;
    	if ($rooms){
    	foreach ($rooms as $room) 
        	{
        		if (!(strtotime($start) < strtotime($room['booking_start']) && strtotime($finish) < strtotime($room['booking_start']) or strtotime($start) >= strtotime($room['booking_finish']) && strtotime($finish) >= strtotime($room['booking_finish'])) ) 
        		{
        			$res = false;
        		}
        	}
        }

        return $res;
    }



    /**
     * @param  $data, $booking_processes_id
     * @return response for set Partners into partners DB table 
     */


    public static function setPartners($data, $booking_processes_id)
    {
    	//global $first;

    	if(count($data)==0)
    		return;
    	
    	$first = $data[0];
    	if(empty($first))
    		return;
    	$partner = new Partner;
    	$partner->name = $first;
    	$partner->room_reservation_id = $booking_processes_id;
    	$partner->save();

    	array_splice($data, 0, 1);

    	Helper::setPartners($data, $booking_processes_id);
    	return true;
    }



    public static function changeStatus($data)
    {
        //global $count;

        if(count($data)==0)
            return;
        $count = $data[0]->roomreservation;
        if(empty($count))
            return;
        foreach ($count as $key => $value) {
            if(Carbon::now()->timestamp >= strtotime($value->booking_finish))
            {
                $value->status = 0;
                $value->save(); 
                continue;
            } 
        }
        
        $data->splice(0, 1);
        Helper::changeStatus($data);

        return true;
    }
    //if(strtotime($start) > strtotime(Carbon::today()) && strtotime($start) < strtotime(Carbon::tomorrow()))
    public static function isToday($start, $finish)
    {
        if(strtotime($start) < Carbon::tomorrow()->timestamp &&  strtotime($finish) < Carbon::tomorrow()->timestamp)
            return true;
        return false;
    }


    public static function isNow($start, $finish)
    {
        if (strtotime($start) <= Carbon::now()->timestamp && strtotime($finish) > Carbon::now()->timestamp)
            return true;
        return false;
    }

    
}
