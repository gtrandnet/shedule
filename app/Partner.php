<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['room_resarvatin_id', 'name'];

    public function reservationRooms(){
    	return $this->belongsTo(BookingProcess::class);
    }
}
